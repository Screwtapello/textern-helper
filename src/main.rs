use crossbeam_channel;
use dirs_next;
use serde::Deserialize;
use serde::Serialize;
use std::env;
use std::ffi;
use std::fmt;
use std::fs;
use std::io;
use std::path;
use std::process;
use std::thread;
use std::time;

mod native_messaging;

fn deserialise_command_template<'de, D>(
    deserialiser: D,
) -> Result<Vec<String>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    struct Visitor;
    impl<'de> serde::de::Visitor<'de> for Visitor {
        type Value = Vec<String>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            write!(formatter, "a string")
        }

        fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
        where
            E: serde::de::Error,
        {
            serde_json::from_slice(v.as_bytes()).map_err(E::custom)
        }
    }

    deserialiser.deserialize_str(Visitor)
}

#[derive(
    Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize,
)]
struct TexternPrefs {
    #[serde(deserialize_with = "deserialise_command_template")]
    #[serde(rename = "editor")]
    command_template: Vec<String>,
    extension: String,
}

#[derive(
    Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize,
)]
#[serde(tag = "type", content = "payload")]
enum Request {
    #[serde(rename = "new_text")]
    NewText {
        id: String,
        text: String,
        caret: usize,
        url: String,
        prefs: TexternPrefs,
    },
}

#[derive(
    Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize,
)]
#[serde(tag = "type", content = "payload")]
enum Response {
    #[serde(rename = "text_update")]
    TextUpdate { id: String, text: String },

    #[serde(rename = "death_notice")]
    DeathNotice { id: String },

    #[serde(rename = "error")]
    Error { error: String },
}

fn write_temp_file(
    tempdir: path::PathBuf,
    url: &str,
    id: &str,
    extension: &str,
    text: &str,
) -> io::Result<path::PathBuf> {
    use std::io::Write;

    let mut res = tempdir;
    res.push(format!("{}-{}.{}", urlencoding::encode(url), id, extension));

    let mut handle = fs::File::create(&res)?;
    handle.write_all(text.as_bytes())?;

    Ok(res)
}

fn calculate_line_and_column(text: &str, caret: usize) -> (usize, usize) {
    let mut offset: usize = 0;
    let mut lines: usize = 0;

    // We don't use .lines() here because it treats `\r\n` the same as '\n`,
    // and that would break our offset counting.
    for line in text.split('\n') {
        if offset + line.len() >= caret {
            return (lines, (caret - offset));
        }

        offset += line.len() + 1; // include the newline
        lines += 1;
    }

    (lines, 0)
}

fn build_command_line<'a, I>(
    template: I,
    tempfile: ffi::OsString,
    line0: usize,
    column0: usize,
) -> Vec<ffi::OsString>
where
    I: Iterator<Item = &'a String>,
{
    let mut res = vec![];
    let mut used_path = false;
    let line0_text = format!("{}", line0);
    let line1_text = format!("{}", line0 + 1);
    let column0_text = format!("{}", column0);
    let column1_text = format!("{}", column0 + 1);

    for each in template {
        // We do all these first, because they're integers
        // and therefore cannot introduce new '%' characters into the arg.
        let each = each.replace("%L", &line0_text);
        let each = each.replace("%l", &line1_text);
        let each = each.replace("%C", &column0_text);
        let each = each.replace("%c", &column1_text);

        // Because tempfile is not a String, we can't just use .replace()
        // to interpolate it into the arg. Instead, we have to do a little
        // dance with split and join. Except we can't .join() either
        // (it's not defined for OsString) so we have to implement that
        // ourselves.
        let mut new_arg = ffi::OsString::new();

        let mut chunks =
            each.split("%s").map(|e| -> ffi::OsString { e.into() });

        if let Some(c) = chunks.next() {
            new_arg.push(c)
        }

        for chunk in chunks {
            used_path = true;
            new_arg.push(&tempfile);
            new_arg.push(chunk);
        }

        res.push(new_arg);
    }

    if !used_path {
        res.push(tempfile);
    }

    res
}

fn handle_message(
    requests: crossbeam_channel::Receiver<Request>,
    responses: crossbeam_channel::Sender<Response>,
) -> io::Result<()> {
    for request in requests.iter() {
        match request {
            Request::NewText {
                id,
                text,
                caret,
                url,
                prefs,
            } => {
                let tempdir =
                    dirs_next::runtime_dir().unwrap_or_else(env::temp_dir);
                let path = write_temp_file(
                    tempdir,
                    &url,
                    &id,
                    &prefs.extension,
                    &text,
                )?;
                let (line0, column0) = calculate_line_and_column(&text, caret);
                let command_line = build_command_line(
                    prefs.command_template.iter(),
                    path.clone().into(),
                    line0,
                    column0,
                );

                eprintln!("Launching editor: {:?}", command_line);
                let mut editor_proc = process::Command::new(&command_line[0])
                    .args(&command_line[1..])
                    .stdin(process::Stdio::null())
                    .stdout(process::Stdio::null())
                    .spawn()?;

                let editor_responses = responses.clone();

                thread::spawn(move || {
                    let path = path;
                    let mut last_mtime = time::SystemTime::now();

                    loop {
                        thread::sleep(time::Duration::from_secs(1));

                        let stats = match fs::metadata(&path) {
                            Ok(data) => data,
                            Err(e) => {
                                eprintln!("Could not stat {:?}: {:?}", path, e);
                                break;
                            }
                        };
                        let new_mtime = match stats.modified() {
                            Ok(mtime) => mtime,
                            Err(e) => {
                                eprintln!(
                                    "Could not get mtime of {:?}: {:?}",
                                    path, e
                                );
                                break;
                            }
                        };

                        if new_mtime != last_mtime {
                            eprintln!("mtime changed!");

                            use std::io::Read;
                            let mut text = String::new();

                            // read the file and send Response::TextUpdate
                            eprintln!("Reading content of {:?}", path);
                            fs::File::open(&path)
                                .and_then(|mut handle| {
                                    handle.read_to_string(&mut text)
                                })
                                .expect("Could not read updated text?");

                            let response = Response::TextUpdate {
                                id: id.clone(),
                                text: text,
                            };
                            editor_responses
                                .send(response)
                                .expect("Could not send update notice?");

                            last_mtime = new_mtime;

                            // Note that if we detect an mtime update, we should
                            // go back to sleep immediately rather than also
                            // poll for child-exited - if the editor does a
                            // save, then a save-and-exit, we might notice the
                            // exit before the final save, and hence miss an
                            // update.
                            continue;
                        }

                        match editor_proc.try_wait() {
                            Ok(Some(status)) => {
                                eprintln!(
                                    "Editor exited with code {:?}",
                                    status.code(),
                                );
                                editor_responses
                                    .send(Response::DeathNotice { id })
                                    .expect("Could not send death notice?");
                                break;
                            }
                            Ok(None) => (), // child has not exited yet
                            Err(e) => {
                                eprintln!("Could not wait for editor: {}", e);
                                break;
                            }
                        }
                    }
                });
            }
        }
    }
    Ok(())
}

fn display_version() {
    println!(
        "{}, version {}",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );
}

fn display_usage() {
    display_version();
    println!();
    println!("A helper to launch external text editors.");
    println!();

    let executable = env::current_exe()
        .expect("Could not determine executable path?")
        .file_name()
        .map(|file| file.to_string_lossy().into_owned())
        .unwrap_or("<executable>".into());

    println!("Usage:");
    println!();
    println!("    {} [OPTIONS]", executable);
    println!("    {} COMMAND", executable);
    println!();
    println!("Options:");
    println!();
    println!("    -h, --help     Display this help message, then exit");
    println!("    -V, --version  Display the software version, then exit");
    println!();
    println!("Commands:");
    println!();
    println!("    install        Make this helper available to web extensions");
    println!();
}

fn main() {
    let args: Vec<ffi::OsString> = env::args_os().collect();

    if args.len() < 2 {
        display_usage();
        return;
    }

    if &args[1] == "-h" || &args[1] == "--help" {
        display_usage();
        return;
    }

    if &args[1] == "-V" || &args[1] == "--version" {
        display_version();
        return;
    }

    let manager = native_messaging::Manager::new(
        "textern".into(),
        "Edit text with an external editor.".into(),
        vec!["textern@jlebon.com".into()],
    );

    if &args[1] == "install" {
        let home = dirs_next::home_dir()
            .expect("Could not determine user home directory");
        let config = dirs_next::config_dir()
            .expect("Could not determine user config directory");
        let executable =
            env::current_exe().expect("Could not determine executable name");

        manager
            .install_manifest(&home, &config, &executable)
            .expect("Could not write manifest");

        return;
    }

    eprintln!(
        "Launched {:?}",
        env::current_exe().expect("Could not determine executable name")
    );

    manager
        .pump_messages(io::stdin(), io::stdout(), handle_message)
        .expect("Message pumping failed?");
}

#[cfg(test)]
mod test {
    use super::*;
    use serde_json;

    #[test]
    fn deserialise_new_text() {
        let raw_message = r##"
            {
                "type": "new_text",
                "payload": {
                    "id": "some_id",
                    "text": "text goes here",
                    "caret": 4,
                    "url": "example.com/some-path",
                    "prefs": {
                        "editor": "[\"vim\", \"+%l\"]",
                        "extension": "txt"
                    }
                }
            }
        "##;

        let message: Request =
            serde_json::from_str(&raw_message).expect("Could not deserialize");

        match message {
            Request::NewText {
                id,
                text,
                caret,
                url,
                prefs,
            } => {
                assert_eq!(id, "some_id");
                assert_eq!(text, "text goes here");
                assert_eq!(caret, 4);
                assert_eq!(url, "example.com/some-path");
                assert_eq!(
                    prefs,
                    TexternPrefs {
                        command_template: vec!["vim".into(), "+%l".into()],
                        extension: "txt".into(),
                    }
                );
            }
        }
    }

    #[test]
    fn line_0_beginning() {
        let (line0, column0) = calculate_line_and_column("hello\nworld\n", 0);
        assert_eq!(line0, 0);
        assert_eq!(column0, 0);
    }

    #[test]
    fn line_0_end() {
        let (line0, column0) = calculate_line_and_column("hello\nworld\n", 5);
        assert_eq!(line0, 0);
        assert_eq!(column0, 5);
    }

    #[test]
    fn line_1_beginning() {
        let (line0, column0) = calculate_line_and_column("hello\nworld\n", 6);
        assert_eq!(line0, 1);
        assert_eq!(column0, 0);
    }

    #[test]
    fn line_1_end() {
        let (line0, column0) = calculate_line_and_column("hello\nworld\n", 11);
        assert_eq!(line0, 1);
        assert_eq!(column0, 5);
    }

    #[test]
    fn line_2_beginning() {
        let (line0, column0) = calculate_line_and_column("hello\nworld\n", 12);
        assert_eq!(line0, 2);
        assert_eq!(column0, 0);
    }

    #[test]
    fn command_with_percent_s() {
        let template: Vec<String> = vec!["-x-%s-x-".into()];
        let tempfile: ffi::OsString = "tempfile".into();

        let expected: Vec<ffi::OsString> = vec!["-x-tempfile-x-".into()];
        let actual = build_command_line(template.iter(), tempfile, 0, 1);

        assert_eq!(actual, expected);
    }

    #[test]
    fn command_without_percent_s() {
        let template: Vec<String> = vec!["gvim".into(), "-f".into()];
        let tempfile: ffi::OsString = "tempfile".into();

        let expected: Vec<ffi::OsString> =
            vec!["gvim".into(), "-f".into(), "tempfile".into()];
        let actual = build_command_line(template.iter(), tempfile, 0, 1);

        assert_eq!(actual, expected);
    }
}
