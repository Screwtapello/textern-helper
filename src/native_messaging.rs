use crossbeam_channel;
use serde::Serialize;
use std::fs;
use std::io;
use std::mem;
use std::path;
use std::thread;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize)]
struct NativeManifest<'a> {
    name: &'a str,
    description: &'a str,
    path: &'a path::Path,
    r#type: &'a str,
    allowed_extensions: &'a Vec<String>,
}

fn write_manifest_to(
    manifest: &NativeManifest,
    mut path: path::PathBuf,
) -> io::Result<()> {
    eprintln!("Creating manifest path: {:?}", path);
    fs::create_dir_all(&path)?;

    path.push(format!("{}.json", manifest.name));

    eprintln!("Writing manifest to {:?}", path);
    serde_json::ser::to_writer(fs::File::create(&path)?, &manifest)?;

    Ok(())
}

pub struct Manager {
    app_name: String,
    app_description: String,
    allowed_extensions: Vec<String>,
}

impl Manager {
    pub fn new(
        app_name: String,
        app_description: String,
        allowed_extensions: Vec<String>,
    ) -> Manager {
        Manager {
            app_name,
            app_description,
            allowed_extensions,
        }
    }

    pub fn install_manifest(
        &self,
        home: &path::Path,
        config_path: &path::Path,
        executable: &path::Path,
    ) -> io::Result<()> {
        let manifest = NativeManifest {
            name: &self.app_name,
            description: &self.app_description,
            path: executable,
            r#type: "stdio",
            allowed_extensions: &self.allowed_extensions,
        };

        // Firefox manifest path from
        // https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_manifests
        let mut manifest_path = path::PathBuf::new();
        manifest_path.push(home);
        manifest_path.push(".mozilla/native-messaging-hosts");
        write_manifest_to(&manifest, manifest_path)?;

        // Chrome manifest path from
        // https://developer.chrome.com/extensions/nativeMessaging#native-messaging-host-location
        let mut manifest_path = path::PathBuf::new();
        manifest_path.push(config_path);
        manifest_path.push("google-chrome/NativeMessagingHosts");
        write_manifest_to(&manifest, manifest_path)?;

        // Chromium manifest path from
        // https://developer.chrome.com/extensions/nativeMessaging#native-messaging-host-location
        let mut manifest_path = path::PathBuf::new();
        manifest_path.push(config_path);
        manifest_path.push("chromium/NativeMessagingHosts");
        write_manifest_to(&manifest, manifest_path)?;

        Ok(())
    }

    pub fn pump_messages<R, S, I, O, F>(
        &self,
        mut input: I,
        mut output: O,
        handler: F,
    ) -> io::Result<()>
    where
        R: serde::de::DeserializeOwned + Send + 'static,
        S: serde::Serialize + Send + 'static,
        F: Fn(
                crossbeam_channel::Receiver<R>,
                crossbeam_channel::Sender<S>,
            ) -> io::Result<()>
            + Send
            + 'static,
        I: io::Read + Send + 'static,
        O: io::Write + Send + 'static,
    {
        let (request_sender, request_receiver) =
            crossbeam_channel::bounded::<R>(0);
        let (response_sender, response_receiver) =
            crossbeam_channel::bounded::<S>(0);

        let request_decoder: thread::JoinHandle<io::Result<()>> =
            thread::spawn(move || {
                let mut buf = vec![];

                loop {
                    use std::convert::TryInto;

                    buf.resize(mem::size_of::<u32>(), 0);
                    if let Err(e) =
                        input.read_exact(&mut buf[..mem::size_of::<u32>()])
                    {
                        match e.kind() {
                            io::ErrorKind::UnexpectedEof => {
                                // We're done here.
                                break;
                            }
                            _ => return Err(e),
                        }
                    }

                    let request_size = u32::from_ne_bytes(
                        buf[..mem::size_of::<u32>()].try_into().expect(
                            "Couldn't convert byte slice to byte array???",
                        ),
                    ) as usize;

                    buf.resize(request_size, 0);
                    if let Err(e) = input.read_exact(&mut buf[..request_size]) {
                        match e.kind() {
                            io::ErrorKind::UnexpectedEof => {
                                eprintln!("Browser sent incomplete request!");
                                break;
                            }
                            _ => return Err(e),
                        }
                    }

                    let request: R = serde_json::from_slice(&buf)?;

                    if let Err(_) = request_sender.send(request) {
                        eprintln!(
                            "Shutting down decoder, since handler vanished"
                        );
                        break;
                    }
                }

                Ok(())
            });

        let response_encoder: thread::JoinHandle<io::Result<()>> =
            thread::spawn(move || {
                for response in response_receiver.iter() {
                    let buf = serde_json::to_vec(&response)?;

                    output.write_all(&(buf.len() as u32).to_ne_bytes())?;
                    output.write_all(&buf)?;
                    output.flush()?;
                }
                Ok(())
            });

        handler(request_receiver, response_sender)
            .expect("Handler failed to exit cleanly?");

        request_decoder
            .join()
            .expect("decoder failed to exit cleanly?")
            .unwrap();
        response_encoder
            .join()
            .expect("encoder failed to exit cleanly?")
            .unwrap();
        Ok(())
    }
}
