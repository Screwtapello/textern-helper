textern-helper
==============

[Textern][tsource] is a Firefox add-on
that allows you to edit text areas in webpage
in the standalone editor of your choice.
It's similar to the now-defunct *It's All Text* add-on,
if you recall that.

Due to the security architecture of modern WebExtensions-based add-ons,
Textern comes in two parts:

  - an addon [in the Firefox addon repository][taddon]
    that can be installed with a single click, that talks to
  - a Python helper script that actually launches your editor
    with a somewhat delicate and error-prone installation procedure

`textern-helper` is a replacement for the Python helper script
that is easier to install and configure.

[taddon]: https://addons.mozilla.org/en-US/firefox/addon/textern/
[tsource]: https://github.com/jlebon/textern

Installation
============

To install Textern with textern-helper,
you must be using Firefox on x86_64 Linux.
Follow these steps:

 1. Install [the Textern add-on][taddon] from the Firefox add-on store
      - This version of textern-helper is designed to work
        with version 0.7.0 of the add-on
 2. Download [the latest build of textern-helper][binary],
    and extract it to somewhere convenient -
    it does not need to be on your $PATH
 3. In a terminal, run `path/to/textern-helper install`
    to make it available to the Textern add-on

Once textern-helper is installed,
you may configure Textern and use it normally.

[binary]: https://gitlab.com/Screwtapello/textern-helper/-/jobs/artifacts/master/download?job=linux-x86_64-build
