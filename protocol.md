Textern's native helper protocol
================================

Notes on the messages Textern sends to the native helper,
and the messages it expects to receive in return.

New Text
--------

Sent from the browser to the helper,
when the user tries invokes
the "edit in external editor" command.

  - `type`: Set to "new_text"
  - `payload`
      - `id`: String, an arbitrary identifier for a specific text field
      - `text`: String, the current content of the text field
      - `caret`: Integer, the index of the character
        immediately after the caret
      - `url`: String, part of the URL of the page containing the text field
      - `prefs`
          - `editor`: String, how to launch the user's preferred editor
          - `extension`: String, the file-extension to use when writing the
            text to a temporary file

The `editor` field is a string,
but it contains a JSON-encoded list of strings,
a command and extra arguments.
Inside each argument, the folllowing escape-sequences are supported:

  - `%s` is replaced with the path to the file to be opened
  - `%L` is replaced with the line the cursor is on (0-based)
  - `%l` is replaced with the line the cursor is on (1-based)
  - `%C` is replaced with the column the cursor is on (0-based)
  - `%c` is replaced with the column the cursor is on (1-based)

If no argument includes `%s`,
the path is added as a separate argument at the end of the command-line.

Text Update
-----------

Sent from the helper to the browser,
when it detects the editor has overwritten the temporary file.

  - `type`: Set to "text_update"
  - `payload`
      - `id`: The unique identifier from the "New Text" message that created
        this file
      - `text`: The newly-written content of the temporary file


Death Notice
------------

Sent from the helper to the browser,
when it detects the editor it launched has quit.

  - `type`: Set to "death_notice"
  - `payload`
      - `id`: The unique identifier from the "New Text" message that created
        this file

Error
-----

Sent from the helper to the browser,
when an error occurs.
The browser extension displays this error directly to the user.

  - `type`: Set to "error"
  - `payload`
      - `error`: String, a human-readable description of the error
